#include <rgb_lcd.h>

rgb_lcd lcd;

using namespace std;
int adc0 = A0;    // select the input pin for the potentiometer
int adc1 = A1;    // select the input pin for the potentiometer
int adc2 = A2;    // select the input pin for the potentiometer
int adc3 = A3;    // select the input pin for the potentiometer

int adc[4] = {0}; // initial sound sensor input
int count[12] = {0};  // count for 12 position
int loopcount = 0;  // loopcount for rotation


long int micros_time2;
long int micros_time1;

long int this_time;


int buttonState = 0;         // variable for reading the pushbutton status
bool lightstate = false;

const int ptm = A4;


byte buttonPin = 2;     // the number of the pushbutton pin
byte lightsensor = 3;
byte directionPin = 9;
byte stepPin = 8;
byte sleepPin = 10;
int pulseWidthMicros = 2;  // microseconds
int microsbetweenSteps = 3600; // milliseconds - or try 1000 for slower steps

int pos = 0;






void setup() {  
  
  pinMode(lightsensor, INPUT);
  pinMode(directionPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(sleepPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);
  lcd.begin(16, 2);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

 
 }

void loop() 
{
  Serial.print("\nSpinning: ");
  Serial.print(microsbetweenSteps);
  for(int i = 0; i<200;i++)
  {
    single_step_motor();
  }
  microsbetweenSteps = microsbetweenSteps - 10;
  if(microsbetweenSteps<1000)microsbetweenSteps = 3600;
  
}


void single_step_motor()
{

  //delay(20);
  //delayMicroseconds(10000);
  digitalWrite(stepPin, HIGH);
  //delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
  digitalWrite(stepPin, LOW);
  delayMicroseconds(microsbetweenSteps);

}



