#include <Wire.h>
#include <rgb_lcd.h>

rgb_lcd lcd;

using namespace std;
int adc0 = A0;    // select the input pin for the potentiometer
int adc1 = A1;    // select the input pin for the potentiometer
int adc2 = A2;    // select the input pin for the potentiometer
int adc3 = A3;    // select the input pin for the potentiometer

int adc[4] = {0}; // initial sound sensor input
int count[12] = {0};  // count for 12 position
int loopcount = 0;  // loopcount for rotation


long int micros_time2;
long int micros_time1;

long int this_time;


byte directionPin = 9;
byte stepPin = 8;
byte sleepPin = 10;
int pulseWidthMicros = 20;  // microseconds
int microsbetweenSteps = 1000; // milliseconds - or try 1000 for slower steps

int pos = 0;






void setup() {  
  
  pinMode(directionPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(sleepPin, OUTPUT);
  Serial.begin(9600);
  lcd.begin(16, 2);

  }

void loop() 
{
  loopcount++;
  read_analog_values();
  rotate_sound(adc);
  
  if (loopcount == 20)
  {
    int r = calpos();
    lcd.print("The position is");
    if (r >= 0)
      rotate(r);

    for(int i = 0; i != 12; i++)  // reset count;
      count[i] = 0;
    loopcount = 0;  // reset loopcount
  }

  /*
  Serial.print(adc[0]);
  Serial.print(" ");
  Serial.print(adc[1]);
  Serial.print(" ");
  Serial.print(adc[2]);
  Serial.print(" ");
  Serial.print(adc[3]);
  Serial.print("\n");
  */

  delay(3);
}


void read_analog_values ()
{
  this_time = millis();
  while(millis()-this_time<100)
  {
    adc[0] = 50 * analogRead(adc0);
    adc[1] = 50 * analogRead(adc1);
    adc[2] = 50 * analogRead(adc2);
    adc[3] = 50 * analogRead(adc3);
  }

//    Serial.print(adc[0]);
//    Serial.print("   ");
//    Serial.print(adc[1]);
//    Serial.print("   ");
//    Serial.print(adc[2]);
//    Serial.print("   ");
//    Serial.print(adc[3]);
//    Serial.print("   \n");

  
}



void rotate_button(int ain)
{
  ain = ain / 20; // resize ain as step number
  rotate(ain);
}

void rotate(int target_pos)
{
  int r = target_pos - pos; // r is the steps need to go
  pos = target_pos;
  Serial.print("next position (unit: step): ");
  Serial.print(target_pos);
  Serial.print("record for current_pos: ");
  Serial.print(pos);
  Serial.print("  \n");
  //Serial.print("next position (unit: step): %d, record for current_pos: %d \n", target_pos, pos); 

  if (r <= 0)
    step_motor_ccw(abs(r));
  else
    step_motor_cw(r);

  /*
  if ( r <= 0)
  {
    if (abs(r) < 100)
    {
      step_motor_ccw(abs(r));
    }
    else 
    {
      r = r + 200;
      step_motor_cw(r);
    }
  }
  else 
  {
    if (r <= 100)
    {
      step_motor_cw(r);
    }
    else
    {
      r = 200 - r;
      step_motor_ccw(r);
    }
  }

  */
}

// pass four sound sensor ADS values
int rotate_sound(int s[])
{
  int r;
  double var = 0.1;

  int v[4] = {0};
  for (int i = 0; i < 4; i++)
    v[i] = s[i];
    
  for (int i = 0; i < 3; i++)
    for (int j = i+1; j < 4; j++)
      if (v[i] <= v[j])
      {
        int tmp = v[j];
        v[j] = v[i];
        v[i] = tmp;
      }

  if (v[0] <= 100) 
  {
    //Serial.print(-1);
    return -1;  // max value less than 10, noise cut
  }

  /*

  double data = ((double)abs(v[2] - v[1])) / v[0];
  Serial.print(data);
  Serial.print("  \n");
  */


  if (((double)abs(v[2] - v[1]) / v[0]) <= var) // rotate to v[0] position
  {
    if (v[0] == s[0])
      count[0]++; // r = 0
    else if (v[0] == s[1])
      count[3]++; // r = 50
    else if (v[0] == s[2])
      count[6]++; // r = 100;
    else if (v[0] == s[3])
      count[9]++; //r = 150
  }
 
 else  // rotate between v[0] and v[1]
  {
    if (v[0] == s[0] && v[1] == s[1])
      count[1]++; //r = 17; 30 = 1.8 * 16.67
    else if (v[0] == s[0] && v[1] == s[3])
      count[11]++; //r = 183; 200 -17
    else if (v[0] == s[1] && v[1] == s[0])
      count[2]++; // r = 34; 50 -16
    else if (v[0] == s[1] && v[1] == s[2])
      count[4]++; // r = 67; 50 +17
    else if (v[0] == s[2] && v[1] == s[1])
      count[5]++; // r = 83;
    else if (v[0] == s[2] && v[1] == s[3])
      count[7]++; // r = 117;
    else if (v[0] == s[3] && v[1] == s[2])
      count[8]++; // r = 133;
    else if (v[0] == s[3] && v[1] == s[0])
      count[10]++; // r = 167;
    else 
    Serial.print(-2);
  }

  return 0;
}

void step_motor_cw(int steps) { 
  digitalWrite(sleepPin, HIGH);
  digitalWrite(directionPin, HIGH);
  for(int n = 0; n < steps; n++)
  {
    digitalWrite(stepPin, HIGH);
    //delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(stepPin, LOW);
    delayMicroseconds(microsbetweenSteps);
  }
  digitalWrite(sleepPin, LOW);
}

void step_motor_ccw(int steps) { 
  digitalWrite(sleepPin, HIGH);
  digitalWrite(directionPin, LOW);
  for(int n = 0; n < steps; n++)
  {
    digitalWrite(stepPin, HIGH);
    //delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(stepPin, LOW);
    delayMicroseconds(microsbetweenSteps);
  }
  digitalWrite(sleepPin, LOW);
}

int calpos()
{
  int m = count[0];
  int it = 0; // point to the current max value in loop

  //  get the max value of count
  for (int i = 1; i != 12; i++)
  {
    m = max(m, count[i]);   
    if (m == count[i]) it = i;  // update pointer
  }

  if (m <= 1) return -1;  // too less sample, ingnore

  switch(it)
  {
    case 0: return 0;
    case 1: return 17;
    case 2: return 34;
    case 3: return 50;
    case 4: return 67;
    case 5: return 83;
    case 6: return 100;
    case 7: return 117;
    case 8: return 133;
    case 9: return 150;
    case 10: return 167;
    case 11: return 183;
    default: return -1;
  }

  return -1;  // program should not go to here
}

