/**************************************************************************/
/*
 Distributed with a free-will license.
 Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
 ADS1115
 This code is designed to work with the ADS1115_I2CADC I2C Mini Module available from ControlEverything.com.
 https://www.controleverything.com/content/Analog-Digital-Converters?sku=ADS1115_I2CADC#tabs-0-product_tabset-2
*/
/**************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "ADS1115.h"


int get_adc0_value()
{
	char adc0_config[3]; // = {0};
	adc0_config[0] = 0x01;
	adc0_config[1] = 0xC4;
	adc0_config[2] = 0x83;
	
	write(file, adc0_config, 3);
	sleep(1);

	char reg[1] = {0x00};
	write(file, reg, 1);
	char data[2]={0};
	int raw_adc = 0;
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		printf("Digital Value of Analog Input on AIN0: %d \n", raw_adc);
	}
	return raw_adc;
}