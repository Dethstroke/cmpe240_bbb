/*
 * SimpleGPIO.cpp
 *
 * Modifications by Derek Molloy, School of Electronic Engineering, DCU
 * www.derekmolloy.ie
 * Almost entirely based on Software by RidgeRun:
 *
 * Copyright (c) 2011, RidgeRun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the RidgeRun.
 * 4. Neither the name of the RidgeRun nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY RIDGERUN ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL RIDGERUN BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SimpleGPIO.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>


#include <linux/i2c-dev.h>
#include <sys/ioctl.h>



//includes for analog in
#include <sys/stat.h>
#include <errno.h>

unsigned int SLEEPGPIO = 49;   // GPIO1_28 = (1x32) + 28 = 60
unsigned int DIRGPIO = 48;   // GPIO1_28 = (1x32) + 28 = 60
unsigned int STEPGPIO = 60;   // GPIO1_28 = (1x32) + 28 = 60


void step_motor_cw (int steps)
{
	gpio_export(STEPGPIO);   // The push button switch
	gpio_export(SLEEPGPIO);   // The push button switch
	gpio_export(DIRGPIO);   // The push button switch
	gpio_set_dir(STEPGPIO, OUTPUT_PIN);   // The LED is an output
	gpio_set_dir(DIRGPIO, OUTPUT_PIN);   // The LED is an output
	gpio_set_dir(SLEEPGPIO, OUTPUT_PIN);   // The LED is an output

	
	gpio_set_value(SLEEPGPIO, HIGH);
	gpio_set_value(DIRGPIO, HIGH);

	usleep(1000);
	
	for(int i = 0; i <steps;i++)
	{
	gpio_set_value(STEPGPIO, HIGH);
	usleep(2);
        gpio_set_value(STEPGPIO, LOW);
	usleep(50000);         // on for 200m
	}
	gpio_set_value(SLEEPGPIO, LOW);
	
	gpio_unexport(SLEEPGPIO);     // unexport the LED
	gpio_unexport(DIRGPIO);
	gpio_unexport(STEPGPIO);
	
}

void step_motor_ccw (int steps)
{
	gpio_export(STEPGPIO);   // The push button switch
	gpio_export(SLEEPGPIO);   // The push button switch
	gpio_export(DIRGPIO);   // The push button switch
	gpio_set_dir(STEPGPIO, OUTPUT_PIN);   // The LED is an output
	gpio_set_dir(DIRGPIO, OUTPUT_PIN);   // The LED is an output
	gpio_set_dir(SLEEPGPIO, OUTPUT_PIN);   // The LED is an output

	
	gpio_set_value(SLEEPGPIO, HIGH);
	//usleep(1000);
	gpio_set_value(DIRGPIO, HIGH);
	/usleep(1000);
	for(int i = 0; i<steps;i++)
	{
	gpio_set_value(STEPGPIO, HIGH);
	usleep(2);
        gpio_set_value(STEPGPIO, LOW);
	usleep(2);
	}         // on for 200ms
	gpio_set_value(SLEEPGPIO, LOW);
	
	gpio_unexport(SLEEPGPIO);     // unexport the LED
	gpio_unexport(DIRGPIO);
	gpio_unexport(STEPGPIO);
	
}

char* get_analog_value ()
{

				char cmd[100];
				sprintf(cmd, "sudo sh -c 'echo 'BB-ADC' > /sys/devices/platform/bone_capemgr/slots'");
				system(cmd);
			    const char *fname = "/sys/bus/iio/devices/iio:device0/in_voltage1_raw";
		        int count = 0, fd, len;
		        char *adc = (char *) malloc(sizeof(char) * 5);

		        fd = open(fname, O_RDONLY);
		        if(fd == -1)
				{
					printf("error: %s %d\n", strerror(errno), errno);
		            exit(1);
		        }
 
		        if(count % 10 == 0 && count != 0)	printf("\n");
		                
				len = read(fd, adc, sizeof(adc - 1));
 
		        if(len == -1)
				{
		            close(fd);               
		        }
		        else if(len == 0)
				{
		            printf("%s\n", "buffer is empty");
		        }
		        else
				{
		            adc[len] ='\0';
		            //printf("\n%s ", adc);
		        }
 
		        close(fd);
 
		        return  adc;
}


int create_i2c_bus()

{
		// Create I2C bus
	int file;
	//char *bus = "/dev/i2c-2";
	if ((file = open("/dev/i2c-2", O_RDWR)) < 0) 
	{
		printf("Failed to open the bus. \n");
		exit(1);
	}
	// Get I2C device, ADS1115 I2C address is 0x48(72)
	ioctl(file, I2C_SLAVE, 0x48);
	
	return file;
}


int get_adc0(int file)
{
	
	char adc0_config[3]; // = {0};
	adc0_config[0] = 0x01;
	adc0_config[1] = 0xC4;
	adc0_config[2] = 0xE3;
	
		write(file, adc0_config, 3);
	//sleep(1);

	char reg[1] = {0x00};
	write(file, reg, 1);
	char data[2]={0};
	int raw_adc = 0;
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		//printf("Digital Value of Analog Input on AIN0: %d \n", raw_adc);
	}
	
	
	return raw_adc;
}

int get_adc1(int file)
{
	
	char adc1_config[3]; // = {0};
	adc1_config[0] = 0x01;
	adc1_config[1] = 0xD4;
	adc1_config[2] = 0xE3;
	
	write(file, adc1_config, 3);
	//sleep(1);

	char reg[1] = {0x00};
	write(file, reg, 1);
	char data[2]={0};
	int raw_adc = 0;
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		///printf("Digital Value of Analog Input on AIN0: %d \n", raw_adc);
	}
	
	
	return raw_adc;
}


int get_adc2(int file)
{
	
	char adc2_config[3]; // = {0};
	adc2_config[0] = 0x01;
	adc2_config[1] = 0xE4;
	adc2_config[2] = 0xE3;
	
	
	write(file, adc2_config, 3);
	//sleep(1);

	char reg[1] = {0x00};
	write(file, reg, 1);
	char data[2]={0};
	int raw_adc = 0;
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		//printf("Digital Value of Analog Input on AIN0: %d \n", raw_adc);
	}
	
	
	return raw_adc;
}

int get_adc3(int file)
{
	
	char adc3_config[3]; // = {0};
	adc3_config[0] = 0x01;
	adc3_config[1] = 0xF4;
	adc3_config[2] = 0xE3;
	
	write(file, adc3_config, 3);
	//sleep(1);

	char reg[1] = {0x00};
	write(file, reg, 1);
	char data[2]={0};
	int raw_adc = 0;
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		//printf("Digital Value of Analog Input on AIN0: %d \n", raw_adc);
	}
	
	
	return raw_adc;
}
/****************************************************************
 * gpio_export
 ****************************************************************/
int gpio_export(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);

	return 0;
}

/****************************************************************
 * gpio_unexport
 ****************************************************************/
int gpio_unexport(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_dir
 ****************************************************************/
int gpio_set_dir(unsigned int gpio, PIN_DIRECTION out_flag)
{
	int fd;
	char buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/direction", gpio);

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/direction");
		return fd;
	}

	if (out_flag == OUTPUT_PIN)
		write(fd, "out", 4);
	else
		write(fd, "in", 3);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_value
 ****************************************************************/
int gpio_set_value(unsigned int gpio, PIN_VALUE value)
{
	int fd;
	char buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-value");
		return fd;
	}

	if (value==LOW)
		write(fd, "0", 2);
	else
		write(fd, "1", 2);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_get_value
 ****************************************************************/
int gpio_get_value(unsigned int gpio, unsigned int *value)
{
	int fd;
	char buf[MAX_BUF];
	char ch;

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_RDONLY);
	if (fd < 0) {
		perror("gpio/get-value");
		return fd;
	}

	read(fd, &ch, 1);

	if (ch != '0') {
		*value = 1;
	} else {
		*value = 0;
	}

	close(fd);
	return 0;
}

bool gpio_get_status(unsigned int gpio)
{
	int fd;
	char buf[MAX_BUF];
	char ch;

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_RDONLY);
	if (fd < 0) {
		perror("gpio/get-value");
		return fd;
	}

	read(fd, &ch, 1);

	if (ch != '0') {
		return false;
	} else {
		return true;
	}

	close(fd);
	return false;
}


/****************************************************************
 * gpio_set_edge
 ****************************************************************/

int gpio_set_edge(unsigned int gpio, char *edge)
{
	int fd;
	char buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/edge", gpio);

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-edge");
		return fd;
	}

	write(fd, edge, strlen(edge) + 1);
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_fd_open
 ****************************************************************/

int gpio_fd_open(unsigned int gpio)
{
	int fd;
	char buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_RDONLY | O_NONBLOCK );
	if (fd < 0) {
		perror("gpio/fd_open");
	}
	return fd;
}

/****************************************************************
 * gpio_fd_close
 ****************************************************************/

int gpio_fd_close(int fd)
{
	return close(fd);
}
