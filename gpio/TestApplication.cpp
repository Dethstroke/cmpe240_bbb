/*
 * TestApplication.cpp
 *
 * Copyright Derek Molloy, School of Electronic Engineering, Dublin City University
 * www.derekmolloy.ie
 *
 * YouTube Channel: http://www.youtube.com/derekmolloydcu/
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL I
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 
//includes for gpio
#include <iostream>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include "SimpleGPIO.h"
//includes for ADS115 i2c
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
//includes for analog in
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>

using namespace std;

//unsigned int SLEEPGPIO = 48;   // GPIO1_28 = (1x32) + 28 = 60
//unsigned int DIRGPIO = 60;   // GPIO1_28 = (1x32) + 28 = 60
//unsigned int STEPGPIO = 49;   // GPIO1_28 = (1x32) + 28 = 60
unsigned int ButtonGPIO = 20;   // GPIO0_15 = (0x32) + 15 = 15

int main(int argc, char *argv[]){
	//this will create the i2c bus connnection
	//int file = create_i2c_bus();
	
	//these will return raw_adc values from the i2c bus
	//int adc0;
	//int adc1;
	//int adc2;
	//int adc3;
	
	char *analog_in = get_analog_value();
	printf("\n%s ", analog_in);
		
	cout << "Testing the GPIO Pins" << endl;

        gpio_export(ButtonGPIO);    // The LED
		//gpio_export(STEPGPIO);   // The push button switch
	//	gpio_export(SLEEPGPIO);   // The push button switch
	//	gpio_export(DIRGPIO);   // The push button switch
		
	//	gpio_set_dir(STEPGPIO, OUTPUT_PIN);   // The LED is an output
	//	gpio_set_dir(DIRGPIO, OUTPUT_PIN);   // The LED is an output
	//    gpio_set_dir(SLEEPGPIO, OUTPUT_PIN);   // The LED is an output
	//	gpio_set_dir(ButtonGPIO, INPUT_PIN);   // The push button input
		
	//	gpio_set_value(DIRGPIO, HIGH);
	//	gpio_set_value(SLEEPGPIO, HIGH);
		
	//This should step the motor 200 times
	//while(!gpio_get_status(ButtonGPIO)){

		step_motor_cw(200);
		//usleep(5000000);
			//adc0 = get_adc0(file);
			//adc1= get_adc1(file);
			//adc2= get_adc2(file);
			//adc3= get_adc3(file);
	//gpio_set_value(SLEEPGPIO, HIGH);
           // gpio_set_value(STEPGPIO, HIGH);
		///	usleep(10000);
            //gpio_set_value(STEPGPIO, LOW);
	//		usleep(10000);         // on for 200ms
	//	gpio_set_value(SLEEPGPIO, LOW);
			//printf("\nADC0: %d ADC1: %d ADC2: %d ADC3:%d", adc0, adc1, adc2, adc3);
		
//	}
	
	//this puts the motor driver to sleep so it doesnt overheat
//	gpio_set_value(SLEEPGPIO, LOW);

	cout << "Finished Testing the GPIO Pins" << endl;
//	gpio_unexport(SLEEPGPIO);     // unexport the LED
//	gpio_unexport(DIRGPIO);
//	gpio_unexport(STEPGPIO);
	gpio_unexport(ButtonGPIO);  // unexport the push button
	return 0;
}

