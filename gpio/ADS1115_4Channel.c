// Distributed with a free-will license.
// Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
// ADS1115
// This code is designed to work with the ADS1115_I2CADC I2C Mini Module available from ControlEverything.com.
// https://www.controleverything.com/content/Analog-Digital-Converters?sku=ADS1115_I2CADC#tabs-0-product_tabset-2
// i am hugo, hugo i am
#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>


void main() 
{
	// Create I2C bus
	int file;
	char *bus = "/dev/i2c-2";
	if ((file = open(bus, O_RDWR)) < 0) 
	{
		printf("Failed to open the bus. \n");
		exit(1);
	}
	// Get I2C device, ADS1115 I2C address is 0x48(72)
	ioctl(file, I2C_SLAVE, 0x48);

	// Select configuration register(0x01)
	// AINP = AIN0 and AINN = AIN1, +/- 2.048V
	// Continuous conversion mode, 128 SPS(0x84, 0x83)
	char adc0_config[3]; // = {0};
	char adc1_config[3]; // = {0};
	char adc2_config[3]; // = {0};
	char adc3_config[3]; // = {0};
	
	adc0_config[0] = 0x01;
	adc0_config[1] = 0xC4;
	adc0_config[2] = 0x83;
	
	adc1_config[0] = 0x01;
	adc1_config[1] = 0xD4;
	adc1_config[2] = 0x83;
	
	
	adc2_config[0] = 0x01;
	adc2_config[1] = 0xE4;
	adc2_config[2] = 0x83;
	
	
	adc3_config[0] = 0x01;
	adc3_config[1] = 0xF4;
	adc3_config[2] = 0x83;
	

	write(file, adc0_config, 3);
	sleep(1);

	char reg[1] = {0x00};
	write(file, reg, 1);
	char data[2]={0};
	int raw_adc = 0;
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		printf("Digital Value of Analog Input on AIN0: %d \n", raw_adc);
	}

	
	write(file, adc1_config, 3);
	sleep(1);

	write(file, reg, 1);
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		printf("Digital Value of Analog Input on AIN1: %d \n", raw_adc);
	}


	write(file, adc2_config, 3);
	sleep(1);

	write(file, reg, 1);
	//char data[2]={0};
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		printf("Digital Value of Analog Input on AIN2: %d \n", raw_adc);
	}

	write(file, adc3_config, 3);
	sleep(1);

	write(file, reg, 1);
	//char data[2]={0};
	if(read(file, data, 2) != 2)
	{
		printf("Error : Input/Output Error \n");
	}
	else 
	{
		// Convert the data
		raw_adc = (data[0] * 256 + data[1]);

		// Output data to screen
		printf("Digital Value of Analog Input on AIN3: %d \n", raw_adc);
	}
}
