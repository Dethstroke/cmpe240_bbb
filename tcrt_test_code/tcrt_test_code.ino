

const int tcrt =  11;      // the number of the LED pin

void setup() {
  Serial.begin(9600);
  pinMode(tcrt, INPUT);
}

void loop(){
  Serial.print(digitalRead(tcrt));
  delay(1000);
}
