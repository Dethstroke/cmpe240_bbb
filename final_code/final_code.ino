#include <rgb_lcd.h>

rgb_lcd lcd;

using namespace std;
int adc0 = A0;    // select the input pin for the potentiometer
int adc1 = A1;    // select the input pin for the potentiometer
int adc2 = A2;    // select the input pin for the potentiometer
int adc3 = A3;    // select the input pin for the potentiometer

int adc[4] = {0}; // initial sound sensor input
int count[12] = {0};  // count for 12 position
int loopcount = 0;  // loopcount for rotation


long int micros_time2;
long int micros_time1;

long int this_time;


int buttonState = 0;         // variable for reading the pushbutton status
bool lightstate = false;

const int ptm = A4;


byte buttonPin = 2;     // the number of the pushbutton pin
byte lightsensor = 3;
byte directionPin = 9;
byte stepPin = 8;
byte sleepPin = 10;
int pulseWidthMicros = 10;  // microseconds
int microsbetweenSteps = 6000; // milliseconds - or try 1000 for slower steps

int pos = 0;






void setup() {  
  
  pinMode(lightsensor, INPUT);
  pinMode(directionPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(sleepPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);
  lcd.begin(16, 2);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  go_home();

 
 }

void loop() 
{
  if (digitalRead(buttonPin))
  {

    manual_mode();
 
  }
  loopcount++;
  read_analog_values();
  rotate_sound(adc);
  
  if (loopcount == 100)
  {
    int r = calpos();
    if (r >= 0)
    {
    
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Pos is:");
    lcd.print(" ");
    int degree = r*1.8;
    lcd.print(degree);
   // lcd.print("   \n");
    
    // delay(500);
      rotate(r);
    }

    for(int i = 0; i != 12; i++)  // reset count;
      count[i] = 0;
    loopcount = 0;  // reset loopcount
  }

 

  delay(3);
}


void go_home()
{
  
  int steps = 0;
  bool complete_check = false;
  digitalWrite(sleepPin, HIGH);
  digitalWrite(directionPin, HIGH);
  while(steps<100)
  { 
      if(digitalRead(lightsensor))
   {
      single_step_motor();

      steps++;
    }
    else if (!digitalRead(lightsensor))  // reach the lightsensor
    {


      digitalWrite(directionPin, LOW);
      for(int i = 0; i != 25; i++)
      {
       single_step_motor();
      }
       complete_check = true;
       lcd.setCursor(0, 0);
       lcd.print("I am home");
       pos = 0;
       delay(2000);
       break;
    }
  }

  if(!complete_check) // have not correct position
  {
    steps = 0;
    digitalWrite(sleepPin, HIGH);
    digitalWrite(directionPin, LOW);
    while (steps < 200)
    {
      lightstate = digitalRead(lightsensor);
      if(lightstate)
      {
        
        single_step_motor();
        steps++;
      }
      else
      {

        for(int i = 0; i != 25; i++)
        {
        single_step_motor();
        }
        complete_check = true;
        lcd.setCursor(0, 0);
        lcd.print("I am home");
        pos = 0;
        delay(2000);
        break;
      }
    }
  }

  if(!complete_check)
  {
    lcd.setCursor(0, 0);
    lcd.print("ERROR!  ");
    go_home();
  }
}


void read_analog_values ()
{
  this_time = millis();
  while(millis() - this_time < 10)
  {
    adc[0] = 50 * analogRead(adc0);
    adc[1] = 50 * analogRead(adc1);
    adc[2] = 50 * analogRead(adc2);
    adc[3] = 50 * analogRead(adc3);
  }
}

void rotate(int target_pos)
{
  int r = target_pos - pos; // r is the steps need to go
  pos = target_pos;
  Serial.print("next position (unit: step): ");
  Serial.print(target_pos);
  Serial.print("record for current_pos: ");
  Serial.print(pos);
  Serial.print("  \n");
  //Serial.print("next position (unit: step): %d, record for current_pos: %d \n", target_pos, pos); 


  if (r <= 0)
    step_motor_ccw(abs(r));
  else
    step_motor_cw(r);

}

void rotate_1(int target_pos)
{
  int r = target_pos - pos; // r is the steps need to go
  pos = target_pos;
  //Serial.print("next position (unit: step): ");
 //Serial.print(target_pos);
  //Serial.print("record for current_pos: ");
 //Serial.print(pos);
  //Serial.print("  \n");
  //Serial.print("next position (unit: step): %d, record for current_pos: %d \n", target_pos, pos); 
  if (r <= 0)
    step_motor_ccw(abs(r));
  else
    step_motor_cw(r);

}

// pass four sound sensor ADS values
int rotate_sound(int s[])
{
  int r;
  double var = 0.1;

  int v[4] = {0};
  for (int i = 0; i < 4; i++)
    v[i] = s[i];
    
  for (int i = 0; i < 3; i++)
    for (int j = i+1; j < 4; j++)
      if (v[i] <= v[j])
      {
        int tmp = v[j];
        v[j] = v[i];
        v[i] = tmp;
      }

  Serial.print("\n");
  Serial.print(v[0]);
  if (v[0] <= 1000) 
  {
    
    return -1;  // max value less than 10, noise cut
  }

  /*

  double data = ((double)abs(v[2] - v[1])) / v[0];
  Serial.print(data);
  Serial.print("  \n");
  */


  if (((double)abs(v[2] - v[1]) / v[0]) <= var) // rotate to v[0] position
  {
    if (v[0] == s[0])
      count[0]++; // r = 0
    else if (v[0] == s[1])
      count[3]++; // r = 50
    else if (v[0] == s[2])
      count[6]++; // r = 100;
    else if (v[0] == s[3])
      count[9]++; //r = 150
  }
 
 else  // rotate between v[0] and v[1]
  {
    if (v[0] == s[0] && v[1] == s[1])
      count[1]++; //r = 17; 30 = 1.8 * 16.67
    else if (v[0] == s[0] && v[1] == s[3])
      count[11]++; //r = 183; 200 -17
    else if (v[0] == s[1] && v[1] == s[0])
      count[2]++; // r = 34; 50 -16
    else if (v[0] == s[1] && v[1] == s[2])
      count[4]++; // r = 67; 50 +17
    else if (v[0] == s[2] && v[1] == s[1])
      count[5]++; // r = 83;
    else if (v[0] == s[2] && v[1] == s[3])
      count[7]++; // r = 117;
    else if (v[0] == s[3] && v[1] == s[2])
      count[8]++; // r = 133;
    else if (v[0] == s[3] && v[1] == s[0])
      count[10]++; // r = 167;
    else 
    Serial.print(-2);
  }

  return 0;
}

void manual_mode()
{
    while (digitalRead(buttonPin))
    {
      static int prev_r_value = 0;
      static int ptm_value = 0;
      ptm_value = analogRead(ptm);
      //Serial.print(analogRead(ptm));
      int r = map(ptm_value, 0, 1024, 0, 199);
      if (r!=prev_r_value)
      {
     lcd.clear();
     lcd.setCursor(0, 0);
    lcd.print("Pos is:");
    lcd.print(" ");
    int degree = r*1.8;
    lcd.print(degree);
        rotate(r);
        
      }
      prev_r_value = r;
      //pos = 0; // update postion as 0;
    }
    go_home();  
}

void step_motor_cw(int steps) { 
  digitalWrite(sleepPin, HIGH);
  digitalWrite(directionPin, HIGH);
  //delay(1);
  for(int n = 0; n < steps; n++)
  {
    lightstate = digitalRead(lightsensor);
    // correction at 45 degree
    if (lightstate == 0) // go over 25th step, still need to go for (pos - 25) steps,
    {
      n = steps - (pos - 25);
    }
    digitalWrite(stepPin, HIGH);
    //delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(stepPin, LOW);
    delayMicroseconds(microsbetweenSteps);
  }
  
}

void step_motor_ccw(int steps) { 
  digitalWrite(sleepPin, HIGH);
  digitalWrite(directionPin, LOW);
  //delay(1);
  for(int n = 0; n < steps; n++)
  {
    lightstate = digitalRead(lightsensor);
    // correction at 45 degree 
    if (lightstate == 0) // go over 25th step, still need to go for (25 - pos) steps,
    {
      n = steps - (25 - pos);
    }
    
    digitalWrite(stepPin, HIGH);
    //delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(stepPin, LOW);
    delayMicroseconds(microsbetweenSteps);
  }
  
}

int calpos()
{
  int m = count[0];
  int it = 0; // point to the current max value in loop

  //  get the max value of count
  for (int i = 1; i != 12; i++)
  {
    m = max(m, count[i]);   
    if (m == count[i]) it = i;  // update pointer
  }

  if (m <= 1) return -1;  // too less sample, ingnore

  switch(it)
  {
    case 0: return 0;
    case 1: return 17;
    case 2: return 34;
    case 3: return 50;
    case 4: return 67;
    case 5: return 83;
    case 6: return 100;
    case 7: return 117;
    case 8: return 133;
    case 9: return 150;
    case 10: return 167;
    case 11: return 183;
    default: return -1;
  }

  return -1;  // program should not go to here
}

void single_step_motor()
{

  //delay(20);
  //delayMicroseconds(10000);
  digitalWrite(stepPin, HIGH);
  //delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
  digitalWrite(stepPin, LOW);
  delayMicroseconds(microsbetweenSteps);

}



