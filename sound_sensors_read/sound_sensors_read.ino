

int adc0 = A0;    // select the input pin for the potentiometer
int adc1 = A1;    // select the input pin for the potentiometer
int adc2 = A2;    // select the input pin for the potentiometer
int adc3 = A0;    // select the input pin for the potentiometer

int adc_max = 0;
int adc_peak[4] = {0};
int adc[4] = {0};

long int micros_time2;
long int micros_time1;

long int this_time;


byte directionPin = 9;
byte stepPin = 8;
byte sleepPin = 10;
int pulseWidthMicros = 20;  // microseconds
int microsbetweenSteps = 1000; // milliseconds - or try 1000 for slower steps


void setup() {  
  
  pinMode(directionPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(sleepPin, OUTPUT);
  Serial.begin(9600);
  }

void loop() 
{
  read_analog_values();
  delay(2500);
  step_motor(200);
}



void step_motor(int steps) { 
  digitalWrite(sleepPin, HIGH);
  digitalWrite(directionPin, HIGH);
  for(int n = 0; n < steps; n++)
  {
    digitalWrite(stepPin, HIGH);
    //delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(stepPin, LOW);
    delayMicroseconds(microsbetweenSteps);
  }
  digitalWrite(sleepPin, LOW);
}


void read_analog_values ()
{
  this_time = millis();
  adc[4] = {0};
  adc_max = 0;
  while(millis()-this_time<100)
  {
    adc[0] = analogRead(adc0);
    adc[1] = analogRead(adc1);
    adc[2] = analogRead(adc2);
    adc[3] = analogRead(adc3);
        if(analogRead(adc0)>adc_max)
    {
      adc[0] = analogRead(adc0);
      adc[1] = analogRead(adc1);
      adc[2] = analogRead(adc2);
      adc[3] = analogRead(adc3);
      adc_max = adc[0];
    }

    if(analogRead(adc1)>adc_max)
    {
      
      adc[0] = analogRead(adc0);
      adc[1] = analogRead(adc1);
      adc[2] = analogRead(adc2);
      adc[3] = analogRead(adc3);
      adc_max = adc[1];
      
    }
    if(analogRead(adc2)>adc_max)
    {
      adc[0] = analogRead(adc0);
      adc[1] = analogRead(adc1);
      adc[2] = analogRead(adc2);
      adc[3] = analogRead(adc3);
      adc_max = adc[2];
    }
    if(analogRead(adc3)>adc_max)
    {
      adc[0] = analogRead(adc0);
      adc[1] = analogRead(adc1);
      adc[2] = analogRead(adc2);
      adc[3] = analogRead(adc3);
      adc_max = adc[3];
    }

  }

//    Serial.print(adc[0]);
//    Serial.print("   ");
//    Serial.print(adc[1]);
//    Serial.print("   ");
//    Serial.print(adc[2]);
//    Serial.print("   ");
//    Serial.print(adc[3]);
//    Serial.print("   \n");

  
}
